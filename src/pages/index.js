import React from "react"
import styled, { createGlobalStyle } from "styled-components";
import Sound from 'react-sound';

import Addidas from "../fonts/segment-webfont.woff";

import LogoOrangeFootPark from "../images/orange-foot-park.png";
import LogoPartenaires from "../images/partenaires.jpg";

import SoundStart from "../sounds/start.mp3";
import SoundEnd from "../sounds/end.mp3";

import "./styles.css"

console.log(Addidas);

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Addidas';
    font-style: normal;
    font-weight: normal;
    src: local('Addidas'), url('${Addidas}') format('woff');
  }
  body {
    font-family: 'Addidas';
    font-size: 48px;
  }
`

const CountDown = styled.span`
  font-size: ${props => props.size}px;
  font-family: Addidas;
  color: #000;
`

const Logo = styled.img`
  height: ${props => props.size}%;
`

const COUNTDOWN_TYPES = {
  COUNTDOWN_BIG: 3300,
  COUNTDOWN_SMALL: 300,
};

class IndexPage extends React.Component {

  state = {
    countdownFontSize: 60,
    countdown: 0,
    countdownString: '00:00',
    countdownType: null
  }

  constructor(props) {
    super(props);

    if (typeof window !== 'undefined') {
      this.startSound = new Audio(SoundStart);
      this.endSound = new Audio(SoundEnd);
    }
    
  }

  formatCountdown = (countdown) => {
    if (countdown < 0) {
      return '00:00';
    }
    var minutes = Math.floor(countdown / 60);
    var seconds = countdown - minutes * 60;

    function str_pad_left(string,pad,length) {
      return (new Array(length+1).join(pad)+string).slice(-length);
    }
    
    return str_pad_left(minutes,'0',2)+':'+str_pad_left(seconds,'0',2);    
  }

  updateCountdown = () => {
    const { countdown, countdownType } = this.state;
    let nextCountdown = countdown - 1;
    let nextCountdownType;

    if (countdown <= 0) {
      // reset countdown
      nextCountdownType = countdownType === 'COUNTDOWN_BIG' ? 'COUNTDOWN_SMALL' : 'COUNTDOWN_BIG';
      nextCountdown = COUNTDOWN_TYPES[nextCountdownType];

      if (nextCountdownType === 'COUNTDOWN_BIG') {
        this.startSound.play();
      }

      this.setState({
        countdown: nextCountdown,
        countdownString: this.formatCountdown(nextCountdown),
        countdownType: nextCountdownType
      })
    } else {

      if (countdownType === 'COUNTDOWN_BIG' && nextCountdown === 0) {
        this.endSound.play();
      }
      // decrement countdown
      this.setState({
        countdown: nextCountdown,
        countdownString: this.formatCountdown(nextCountdown),
      })
    }
  }

  toggleStart = () => {
    if (this.timerCountdown) {
      clearInterval(this.timerCountdown);
      this.timerCountdown = null;
    } else {
      this.timerCountdown = setInterval(() => {
        this.updateCountdown();
      }, 1000);
    }
  }

  biggerFont = () => {
    const { countdownFontSize } = this.state;

    let nextFontSize = countdownFontSize + 10;
    if (nextFontSize > 200) {
      nextFontSize = 200;
    }
    this.setState({
      countdownFontSize: nextFontSize
    });
  }

  smallerFont = () => {
    const { countdownFontSize } = this.state;

    let nextFontSize = countdownFontSize - 10;
    if (nextFontSize < 10) {
      nextFontSize = 10;
    }
    this.setState({
      countdownFontSize: nextFontSize
    });
  }

  forcePast = (time) => {
    const { countdown, countdownType } = this.state;
    let nextCountdown = countdown + time;
    nextCountdown = nextCountdown >= COUNTDOWN_TYPES[countdownType] ? COUNTDOWN_TYPES[countdownType] : nextCountdown;
    this.setState({
      countdown: nextCountdown,
      countdownString: this.formatCountdown(nextCountdown),
    })
  }

  forceFuture = (time) => {
    const { countdown } = this.state;
    let nextCountdown = countdown - time;
    nextCountdown = nextCountdown < 0 ? 0 : nextCountdown;
    this.setState({
      countdown: nextCountdown,
      countdownString: this.formatCountdown(nextCountdown),
    })
  }

  render() {
    const { countdownString, countdownFontSize } = this.state;
    return (
      <div style={{ 
        display: 'flex',
        width: '100vw',
        height: "100vh",
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'center'
      }}
    >
      <GlobalStyle />
      <div style={{
          display: 'flex',
          width: '100vw',
          height: "100vh",
          flexDirection: 'column',
          alignItems: 'stretch',
          justifyContent: 'center'
        }}
      >
        <div style={{ 
            flex: 5,
            alignItems: 'center',
            justifyContent: 'center',
            display: 'flex',

          }}
        >
          <CountDown size={countdownFontSize}>
            {countdownString}
          </CountDown>
        </div>
        <div style={{ 
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          padding: '30px',
          height: '20%'
        }}
      >
        <div style={{ 
            flex: 1,
            display: 'flex',
            alignItems: 'flex-end',
            justifyContent: 'flex-start',
            height: '100%'
          }}
        >
          <Logo size={50} src={LogoPartenaires} />
        </div>
        <div style={{ 
            height: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Logo size={100} src={LogoOrangeFootPark} />
        </div>
      </div>
        
        <div 
          onClick={() => this.forceFuture(600)}
          style={{
            position: "absolute",
            top: '0px',
            right: 0,
            width: '33%',
            height: '33%',
          }}
        />
        <div 
          onClick={() => this.forceFuture(60)}
          style={{
            position: "absolute",
            top: '33%',
            right: 0,
            width: '33%',
            height: '33%',
          }}
        />
        <div 
          onClick={() => this.forceFuture(10)}
          style={{
            position: "absolute",
            top: '66%',
            right: 0,
            width: '33%',
            height: '33%',
          }}
        />

        <div 
          onClick={this.biggerFont}
          style={{
            position: "absolute",
            top: '0px',
            left: '33%',
            width: '33%',
            height: '33%',
          }}
        />
        <div 
          onClick={this.toggleStart}
          style={{
            position: "absolute",
            top: '33%',
            left: '33%',
            width: '33%',
            height: '33%',
          }}
        />
        <div 
          onClick={this.smallerFont}
          style={{
            position: "absolute",
            top: '66%',
            left: '33%',
            width: '33%',
            height: '33%',
          }}
        />

        <div 
          onClick={() => this.forcePast(600)}
          style={{
            position: "absolute",
            top: '0px',
            left: 0,
            width: '33%',
            height: '33%',
          }}
        />
        <div 
          onClick={() => this.forcePast(60)}
          style={{
            position: "absolute",
            top: '33%',
            left: 0,
            width: '33%',
            height: '33%',
          }}
        />
        <div 
          onClick={() => this.forcePast(10)}
          style={{
            position: "absolute",
            top: '66%',
            left: 0,
            width: '33%',
            height: '33%',
          }}
        />
      </div>
      </div>
    );
  }
}

export default IndexPage;
